# Seleção Arquitetura

Olá, tudo tranquilo? Se você chegou até aqui, significa que tem interesse em uma das nossas vagas, certo? Basicamente, o seu código será analisado por um dos nossos arquitetos e se você se sair bem, é bem provável que faça parte do nosso time. Desejamos que você seja muito bem sucedido na atividade quem vem a seguir.

## Vagas

Estamos em constante ascenção e temos vagas para todos os perfis.


## 1. Do que se trata a atividade?

Precisamos que você nos mostre que sabe arquitetar sistemas, que possui sólidos conhecimentos em desenvolvimento de software, padrões de projetos, solid e por aí vai.  

Você trabalha com novas e antigas tecnologias? Ok, nesse desafio você utilizará a stack Spring e Angular 7. 

Basicamente a ideia consiste na criação de dois projetos, uma API REST escrita com spring-boot 2.x e banco h2, além de uma camada Web escrita com Angular 7 com webpack. Além disso, use o maven para empacotar esses dois projetos em um único war.


### 2. O que será avaliado e caso não seja feito serei desclassificado?

* Todos os requistos devem ser atendidos
* Uso adequado e justificado da stack spring (Você vai precisar nos dizer o porque de usar uma ou outra solução)
* Uso do padrão arquitetural MVC (no backend e no frontend Angular)
* Padrões de projeto (Você é um arquiteto certo? Então você precisa nos justificar quais foram todos os padrões que você utilizou e porque ele se adequa melhor para o cenário aplicado)
* Uso de Solid
* Perfeito uso da coesão e baixo acoplamento
* Polimorfismo
* Uso do maven
* Uso do webpack
* Restfull
* Documentação interativa da API
* Evitar código extremamente verboso (adote frameworks para isso e defenda a sua escolha)
* Defesa de como você pensou em resolver os requisitos da aplicação. É a melhor forma? Se não é porque qual motivo essa solução foi adotada? 
* Usuário e senha
* Chamadas REST escritas no Visual Code Studio com o plugin RestClient. Iremos fazer chamadas direta a sua API sem o front e não vamos sair montando requisições HTTP para isso. Você precisa entregar pronto cada chamada a cada recurso, lembre da autenticação.
* Front para todos os requisitos com Angular 7, considerando autenticação.
* Nesse sistema terão importações de massa de dados. Utilize a API do Java 8 e faça o processamento de modo paralelo para otimizar o processamento.

### 3. Requisitos que devem ser implementados

* Baixe o arquivo 2018-1_CA.csv por meio do link http://www.anp.gov.br/images/dadosabertos/precos/2018-1_CA.csv e crie uma API REST seguindo os tópicos especificados a seguir
* Implemente uma API que possui autenticação JWT
* Implemente uma documentação interativa. O acesso a essa URI não requer autenticação
* Implementar recurso para CRUD de usuários
* Implementar recurso para CRUD de histórico de preço de combustível
* Implementar recurso para importação de csv
* Implementar recurso que retorne a média de preço de combustível com base no nome do município
* Implementar recurso que retorne todas as informações importadas por sigla da região
* Implementar recurso que retorne os dados agrupados por distribuidora
* Implementar recurso que retorne os dados agrupados pela data da coleta
* Implementar recurso que retorne o valor médio do valor da compra e do valor da venda por município
* Implementar recurso que retorne o valor médio do valor da compra e do valor da venda por bandeira
* OBS: Você deve deduzir quais são as entidades do domínio necessárias para completar a atividade, tal como os relacionamentos, etc


### 5. Uma vez feito, como entrego o projeto?

* Crie um fork dessa avaliação
* Desenvolva
* Envie e-mail para dmoreira@indracompany.com e gsavio@indracompany.com sinalizando a entrega
* Você possui 5 dias para entregar a atividade.


### 6. O que me desclassificaria automaticamente?

* Commitar sem seguir as regras dos tópicos anteriores
* Sua aplicação não subir
* Se sua aplicação não autenticar
* Não entregar a atividade dentro do prazo estabelecido